import Mock from 'mockjs'
import { doCustomTimes } from '@/libs/util'

export const search = (query) => {
  let tableData = []
  doCustomTimes(5, () => {
    tableData.push(Mock.mock({
      'name|1': ['系统管理员', '系统子管理员', '运营专员'],
      'code|1': ['ROLE_ADMIN', 'ROLE_USER', 'USER_MANAGER']
    }))
  })
  return tableData
}
