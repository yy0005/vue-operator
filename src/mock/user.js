import Mock from 'mockjs'
import { doCustomTimes } from '@/libs/util'

export const searchManager = (query) => {
  let tableData = []
  doCustomTimes(5, () => {
    tableData.push(Mock.mock({
      username: '@name',
      'roles|1': ['ROLE_ADMIN', 'ROLE_USER', 'USER_MANAGER']
    }))
  })
  return tableData
}
