import axios from '@/libs/api.request'

export const login = ({ userName, password }) => {
  const data = {
    username: userName,
    password: password
  }
  return axios.request({
    url: 'login',
    params: data,
    method: 'post'
  })
}

export const getUserInfo = token => {
  return axios.request({
    url: 'get_info',
    params: {
      token
    },
    method: 'get'
  })
}

export const logout = token => {
  return axios.request({
    url: 'logout',
    method: 'post'
  })
}

// 添加用户
export const addUser = data => {
  return axios.request({
    url: '/users',
    method: 'post',
    data: data
  })
}

// 用户列表
export const userList = data => {
  return axios.request({
    url: '/users',
    method: 'get',
    params: data
  })
}

// 删除用户列表
export const delUser = id => {
  return axios.request({
    url: `/users/${id}`,
    method: 'delete'
  })
}

// 获取用户信息
export const getUser = id => {
  return axios.request({
    url: `/users/${id}`,
    method: 'get'
  })
}

// 保存编辑后的信息
export const addEditUser = (data, id) => {
  return axios.request({
    url: `/users/${id}`,
    method: 'put',
    data: data
  })
}

// 重置密码
export const resetPass = (id) => {
  return axios.request({
    url: `/users/${id}/pwd/reset/default`,
    method: 'put'
  })
}
// 判读用户是否需要重置密码
export const isReset = id => {
  return axios.request({
    url: `/users/${id}`,
    method: 'get'
  })
}

// 登录的用户进行重置密码
export const resetLogin = data => {
  return axios.request({
    url: '/users/pwd/reset',
    method: 'put',
    params: data
  })
}
