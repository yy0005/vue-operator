import axios from '@/libs/api.request'

// 查询对账单
export const getCode = (data) => {
  return axios.request({
    url: '/storeCheck/getStoreCkeckList',
    method: 'get',
    params: data
  })
}
// 删除对账单信息
export const deletCode = (data) => {
  return axios.request({
    url: '/storeCheck/deleteStoreCkeck',
    method: 'post',
    data
  })
}
// 上传信息
export const uploadCode = (data) => {
  return axios.request({
    url: '/storeCheck/uploadStoreCheckExcel',
    method: 'post',
    data
  })
}

//获取订单列表
export const getIndent = (data) => {
  return axios.request({
    url: '/order/getOrderList',
    method: 'get',
    params: data
  })
}

//导入订单
export const uploadIndent = (data) => {
  return axios.request({
    url: '/order/uploadOrderMappingExcel',
    method: 'post',
    data
  })
}
// 删除订单信息
export const deletIndent = (data) => {
  return axios.request({
    url: '/order/deleteOrder',
    method: 'post',
    data
  })
}


//获取商品列表
export const getProduct = (data) => {
  return axios.request({
    url: '/product/getProductList',
    method: 'get',
    params: data
  })
}

//导入商品
export const uploadProduct = (data) => {
  return axios.request({
    url: '/product/uploadProductMappingExcel',
    method: 'post',
    data
  })
}
// 删除商品信息
export const deletProduct = (data) => {
  return axios.request({
    url: '/product/deleteProduct',
    method: 'post',
    data
  })
}
//新增商品信息
export const addProduct = (data) => {
  return axios.request({
    url: '/product/saveProduct',
    method: 'post',
    data
  })
}
