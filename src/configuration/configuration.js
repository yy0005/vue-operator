import config from '../config/index.js'
let prefix = process.env.NODE_ENV === 'production' ? config.baseUrl.pro : config.baseUrl.dev
export const systemConfig = {
  defaultShopId: 1,
  defaultPageNo: 1,
  defaultPageSize: 10,
  defaultPageSizeOptions: [10, 20, 50, 100]
}
